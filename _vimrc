set nocompatible               " be iMproved
filetype off                   " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
" required! 
Bundle 'gmarik/vundle'

" My Bundles here:
"
" original repos on github
Bundle 'Lokaltog/vim-easymotion'
Bundle 'bling/vim-airline'
Bundle 'scrooloose/nerdtree'
Bundle 'scrooloose/syntastic'
Bundle 'scrooloose/nerdcommenter'
"Bundle 'mileszs/ack.vim'
Bundle 'Raimondi/delimitMate'
Bundle 'docunext/closetag.vim'
Bundle 'mattn/emmet-vim'
Bundle "MarcWeber/vim-addon-mw-utils"
Bundle "tomtom/tlib_vim"
Bundle "tpope/vim-surround"
Bundle "Shougo/unite.vim"
Bundle "Shougo/vimproc.vim"
Bundle 'majutsushi/tagbar'
Bundle 'tpope/vim-fugitive'
Bundle 'xsbeats/vim-blade'
Bundle 'tobyS/vmustache'
Bundle 'tobyS/pdv'
Bundle 'shawncplus/phpcomplete.vim'
Bundle 'airblade/vim-gitgutter'
Bundle 'joonty/vdebug.git'
Bundle 'm2mdas/phpcomplete-extended'
Bundle 'm2mdas/phpcomplete-extended-laravel'
Bundle 'Valloric/YouCompleteMe'
Bundle 'SirVer/ultisnips'
Bundle 'kien/ctrlp.vim'

filetype plugin indent on     " required!
"
" Brief help
" :BundleList          - list configured bundles
" :BundleInstall(!)    - install(update) bundles
" :BundleSearch(!) foo - search(or refresh cache first) for foo
" :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Bundle command are not allowed..

syntax on

set nocompatible   " Disable vi-compatibility
set t_Co=256

set incsearch

colorscheme xoria256
let macvim_skip_colorscheme = 1
if has("gui_running")
    colorscheme xoria256
    set guifont=Monaco:h15
endif

"set guifont=Source\ Code\ for\ Powerline:h16
"set guifont=menlo\ for\ powerline:h16
"set guifont=Monaco:h15
set guioptions-=T " Removes top toolbar
set guioptions-=r " Removes right hand scroll bar
set go-=L " Removes left hand scroll bar
set linespace=8

if has("gui_running")
    set linespace=4
endif

set showmode                    " always show what mode we're currently editing in
set nowrap                      " don't wrap lines
set tabstop=4                   " a tab is four spaces
set smarttab
set tags=tags
set softtabstop=4               " when hitting <BS>, pretend like a tab is removed, even if spaces
set expandtab                   " expand tabs by default (overloadable per file type later)
set shiftwidth=4                " number of spaces to use for autoindenting
set shiftround                  " use multiple of shiftwidth when indenting with '<' and '>'
set backspace=indent,eol,start  " allow backspacing over everything in insert mode
set autoindent                  " always set autoindenting on
set copyindent                  " copy the previous indentation on autoindenting
set number                      " always show line numbers
set ignorecase                  " ignore case when searching
set smartcase                   " ignore case if search pattern is all lowercase,
set timeout timeoutlen=200 ttimeoutlen=100
set visualbell           " don't beep
set noerrorbells         " don't beep
set autowrite  "Save on buffer switch
set mouse=a

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
let mapleader = ","
let g:mapleader = ","

" Fast saves
nmap <leader>s :w!<cr>

" Down is really the next line
nnoremap j gj
nnoremap k gk

"Easy escaping to normal model
imap jj <esc>

"Auto change directory to match current file ,cd
nnoremap ,cd :cd %:p:h<CR>:pwd<CR>

"easier window navigation

nmap <C-h> <C-w>h
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
nmap <C-l> <C-w>l

"Resize vsplit
"nmap <C-v> :vertical resize +5<cr>
nmap 25 :vertical resize 40<cr>
nmap 50 <c-w>=
nmap 75 :vertical resize 180<cr>

" PHPDocumentor
let g:pdv_template_dir = $HOME ."/.vim/bundle/pdv/templates_snip"
nnoremap  <C-O> :call pdv#DocumentWithSnip()<CR>

autocmd BufNewFile,BufRead *.sass             set ft=sass.css

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Unite
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:unite_split_rule = "botright"
"let g:unite_force_overwrite_statusline = 0
"let g:unite_winheight = 15

let g:unite_data_directory='~/.vim/.cache/unite'
let g:unite_enable_start_insert=1
let g:unite_source_history_yank_enable=1
let g:unite_source_rec_max_cache_files=5000
let g:unite_prompt='» '

call unite#custom_source('file_rec,file_rec/async,file_mru,file,buffer,grep',
      \ 'ignore_pattern', join([
      \ '\.git/',
      \ ], '\|'))

call unite#filters#matcher_default#use(['matcher_fuzzy'])
call unite#filters#sorter_default#use(['sorter_rank'])
call unite#set_profile('files', 'smartcase', 1)
call unite#custom#source('line,outline','matchers','matcher_fuzzy')

"nnoremap <C-p> :<C-u>Unite -toggle -auto-resize file_rec/async:! buffer <CR><c-u>

nnoremap <leader>m :<C-u>Unite -quick-match file_mru<CR>
nnoremap <leader><leader>m :<C-u>Unite -quick-match buffer<CR>
nnoremap <leader>y :<C-u>Unite history/yank<CR>
nnoremap <leader>tb :<C-u>Unite -quick-match tab<CR>
nnoremap <leader>fi :<C-u>Unite grep:.<CR>

autocmd FileType unite call s:unite_settings()

function! s:unite_settings()
  let b:SuperTabDisabled=1
  imap <buffer> <C-j>   <Plug>(unite_select_next_line)
  imap <buffer> <C-k>   <Plug>(unite_select_previous_line)
  imap <buffer> <C-l>   <Plug>(unite_redraw)
  imap <silent><buffer><expr> <C-x> unite#do_action('split')
  imap <silent><buffer><expr> <C-v> unite#do_action('vsplit')
  imap <silent><buffer><expr> <C-t> unite#do_action('tabopen')

  nmap <buffer> <ESC> <Plug>(unite_exit)
endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

nmap <C-b> :NERDTreeToggle<cr>
let NERDTreeOptionDetails = 1
let g:NERDChristmasTree = 1
let g:NERDTreeWinSize = 40
let g:NERDTreeQuitOnOpen = 1

"Load the current buffer in Chrome
nmap ,c :!open -a Google\ Chrome<cr>

"Show (partial) command in the status line
set showcmd

" Create split below
nmap :sp :rightbelow sp<cr>

" Quickly go forward or backward to buffer
nmap :bp :BufSurfBack<cr>
nmap :bn :BufSurfForward<cr>

highlight Search cterm=underline

" Swap files out of the project root
set backupdir=~/.vim/backup//
set directory=~/.vim/swap//

" Run PHPUnit tests
"map <Leader>t :!phpunit %<cr>

" Easy motion stuff
let g:EasyMotion_leader_key = '<Leader>'

" Powerline (Fancy thingy at bottom stuff)
let g:Powerline_symbols = 'fancy'
set laststatus=2   " Always show the statusline
set encoding=utf-8 " Necessary to show Unicode glyphs
set noshowmode " Hide the default mode text (e.g. -- INSERT -- below the statusline)
let g:airline_powerline_fonts = 1

autocmd cursorhold * set nohlsearch
autocmd cursormoved * set hlsearch

" Remove search results
command! H let @/=""
" If you prefer the Omni-Completion tip window to close when a selection is
" made, these lines close it on movement in insert mode or when leaving
" insert mode
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif
let g:SuperTabDefaultCompletionType = "context"

" PIV pligun
let g:DisableAutoPHPFolding=1
let php_folding=0

" Abbreviations
abbrev pft PHPUnit_Framework_TestCase

abbrev gm !php artisan generate:model
abbrev gc !php artisan generate:controller
abbrev gmig !php artisan generate:migration

" Auto-remove trailing spaces
autocmd BufWritePre *.php :%s/\s\+$//e

" Edit todo list for project
nmap ,todo :e todo.txt<cr>

" Laravel framework commons
nmap <leader>lr :e app/routes.php<cr>
nmap <leader>lca :e app/config/app.php<cr>81Gf(%O
nmap <leader>lcd :e app/config/database.php<cr>
nmap <leader>lc :e composer.json<cr>

" Concept - load underlying class for Laravel
function! FacadeLookup()
    let facade = input('Facade Name: ')
    let classes = {
\       'Form': 'Html/FormBuilder.php',
\       'Html': 'Html/HtmlBuilder.php',
\       'File': 'Filesystem/Filesystem.php',
\       'Eloquent': 'Database/Eloquent/Model.php'
\   }

    execute ":edit vendor/laravel/framework/src/Illuminate/" . classes[facade]
endfunction
nmap ,lf :call FacadeLookup()<cr>

" CtrlP Stuff

"let g:ctrlp_extensions = ['tag', 'buffertag', 'quickfix', 'dir', 'rtscript', 'undo', 'line', 'changes', 'mixed', 'bookmarkdir']
" Tagbar config
let g:tagbar_autofocus = 1
let g:tagbar_autoclose = 1

" Familiar commands for file/symbol browsing
map <D-p> :CtrlP<cr>
map <C-r> :CtrlPBufTag<cr>

" I don't want to pull up these folders/files when calling CtrlP
set wildignore+=*/vendor/**
set wildignore+=*/public/forum/**

" Open splits
nmap vs :vsplit<cr>
nmap sp :split<cr>

" Create/edit file in the current directory
nmap :ed :edit %:p:h/

nmap <leader>§ <c-y>,
nmap <leader>er :e ~/.vimrc<cr>
nmap <leader><leader>t :TagbarToggle<CR>
nmap <leader>es :e ~/.vim/snippets/php.snippets<cr>
"Git mappings
nmap <leader>gw :Gwrite<cr>:Gcommit
nmap <leader>gr :Gread
nmap <leader>gc :Gcommit
nmap <leader>gs :Gstatus<cr>
nmap <leader>gd :Gdiff<cr>
nmap <leader>go :diffoff<cr>
nmap <leader>br :Breakpoint<cr>
noremap <C-d> :sh<cr>

"Ultisnips
let g:UltiSnipsListSnippets = '<leader>ud'
let g:UltiSnipsExpandTrigger = '<C-e>'

" Vdebug options
let g:vdebug_options = {}
let g:vdebug_options['break_on_open'] = 0
let g:vdebug_options['watch_window_style'] = 'expanded'
let g:vdebug_keymap = {"step_over" : "<F1>", "step_into" : "<F2>", "step_out" : "<F3>", "close" : "<F4>", "set_breakpoint" : "<F6>", }

"vim-seek configuration
let g:seek_enable_jumps = 1

"phpcomplete extended
let g:phpcomplete_index_composer_command='composer'
autocmd  FileType  php setlocal omnifunc=phpcomplete_extended#CompletePHP

"tab mappings
nnoremap <leader>tn :tabnew<CR>
nnoremap <leader>te :tabclose<CR>

set term=xterm-256color
