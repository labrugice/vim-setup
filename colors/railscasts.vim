


<!DOCTYPE html>
<html>
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# githubog: http://ogp.me/ns/fb/githubog#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>vim-railscasts-theme/colors/railscasts.vim at master · jpo/vim-railscasts-theme</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub" />
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub" />
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-144.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144.png" />
    <link rel="logo" type="image/svg" href="https://github-media-downloads.s3.amazonaws.com/github-logo.svg" />
    <meta property="og:image" content="https://github.global.ssl.fastly.net/images/modules/logos_page/Octocat.png">
    <meta name="hostname" content="github-fe123-cp1-prd.iad.github.net">
    <meta name="ruby" content="ruby 1.9.3p194-tcs-github-tcmalloc (0e75de19f8) [x86_64-linux]">
    <link rel="assets" href="https://github.global.ssl.fastly.net/">
    <link rel="conduit-xhr" href="https://ghconduit.com:25035/">
    <link rel="xhr-socket" href="/_sockets" />
    


    <meta name="msapplication-TileImage" content="/windows-tile.png" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="selected-link" value="repo_source" data-pjax-transient />
    <meta content="collector.githubapp.com" name="octolytics-host" /><meta content="collector-cdn.github.com" name="octolytics-script-host" /><meta content="github" name="octolytics-app-id" /><meta content="57C4E3D9:652E:99116F:5276B6AC" name="octolytics-dimension-request_id" /><meta content="1526611" name="octolytics-actor-id" /><meta content="rodrigocruz" name="octolytics-actor-login" /><meta content="06966488b74982c84b9c9b78d0fa4f2b974197e0bdb5e92e6fe2a1a045747ccb" name="octolytics-actor-hash" />
    

    
    
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />

    <meta content="authenticity_token" name="csrf-param" />
<meta content="6OSx6gZsal4pn6o+oVTJLD1runVTn28TL1Pudugt6HE=" name="csrf-token" />

    <link href="https://github.global.ssl.fastly.net/assets/github-82d95d078b16fa64cfcabaff99138f5c59619266.css" media="all" rel="stylesheet" type="text/css" />
    <link href="https://github.global.ssl.fastly.net/assets/github2-b95518dadd9fd57d8fb892270da34baed14530c4.css" media="all" rel="stylesheet" type="text/css" />
    

    

      <script src="https://github.global.ssl.fastly.net/assets/frameworks-3d32afc910800ca0abfc4ed4357ed8a6f369f266.js" type="text/javascript"></script>
      <script src="https://github.global.ssl.fastly.net/assets/github-3ebcae34d9d5b6bd9a5bd8e3f5560c3692877177.js" type="text/javascript"></script>
      
      <meta http-equiv="x-pjax-version" content="6f6817f7edc3e64c7c6a02986ff4c698">

        <link data-pjax-transient rel='permalink' href='/jpo/vim-railscasts-theme/blob/9035daff38dbbd30229890f26e54d9a7a71deca3/colors/railscasts.vim'>
  <meta property="og:title" content="vim-railscasts-theme"/>
  <meta property="og:type" content="githubog:gitrepository"/>
  <meta property="og:url" content="https://github.com/jpo/vim-railscasts-theme"/>
  <meta property="og:image" content="https://github.global.ssl.fastly.net/images/gravatars/gravatar-user-420.png"/>
  <meta property="og:site_name" content="GitHub"/>
  <meta property="og:description" content="vim-railscasts-theme - A port of the Railscasts Textmate theme to Vim"/>

  <meta name="description" content="vim-railscasts-theme - A port of the Railscasts Textmate theme to Vim" />

  <meta content="28533" name="octolytics-dimension-user_id" /><meta content="jpo" name="octolytics-dimension-user_login" /><meta content="61883" name="octolytics-dimension-repository_id" /><meta content="jpo/vim-railscasts-theme" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="61883" name="octolytics-dimension-repository_network_root_id" /><meta content="jpo/vim-railscasts-theme" name="octolytics-dimension-repository_network_root_nwo" />
  <link href="https://github.com/jpo/vim-railscasts-theme/commits/master.atom" rel="alternate" title="Recent Commits to vim-railscasts-theme:master" type="application/atom+xml" />

  </head>


  <body class="logged_in  env-production  vis-public  page-blob">
    <div class="wrapper">
      
      
      
      


      <div class="header header-logged-in true">
  <div class="container clearfix">

    <a class="header-logo-invertocat" href="https://github.com/">
  <span class="mega-octicon octicon-mark-github"></span>
</a>

    
    <a href="/notifications" class="notification-indicator tooltipped downwards" data-gotokey="n" title="You have unread notifications">
        <span class="mail-status unread"></span>
</a>

      <div class="command-bar js-command-bar  in-repository">
          <form accept-charset="UTF-8" action="/search" class="command-bar-form" id="top_search_form" method="get">

<input type="text" data-hotkey="/ s" name="q" id="js-command-bar-field" placeholder="Search or type a command" tabindex="1" autocapitalize="off"
    
    data-username="rodrigocruz"
      data-repo="jpo/vim-railscasts-theme"
      data-branch="master"
      data-sha="5247f6d0e5aa34a9c7aba8e49f059b1f2cd3569b"
  >

    <input type="hidden" name="nwo" value="jpo/vim-railscasts-theme" />

    <div class="select-menu js-menu-container js-select-menu search-context-select-menu">
      <span class="minibutton select-menu-button js-menu-target">
        <span class="js-select-button">This repository</span>
      </span>

      <div class="select-menu-modal-holder js-menu-content js-navigation-container">
        <div class="select-menu-modal">

          <div class="select-menu-item js-navigation-item js-this-repository-navigation-item selected">
            <span class="select-menu-item-icon octicon octicon-check"></span>
            <input type="radio" class="js-search-this-repository" name="search_target" value="repository" checked="checked" />
            <div class="select-menu-item-text js-select-button-text">This repository</div>
          </div> <!-- /.select-menu-item -->

          <div class="select-menu-item js-navigation-item js-all-repositories-navigation-item">
            <span class="select-menu-item-icon octicon octicon-check"></span>
            <input type="radio" name="search_target" value="global" />
            <div class="select-menu-item-text js-select-button-text">All repositories</div>
          </div> <!-- /.select-menu-item -->

        </div>
      </div>
    </div>

  <span class="octicon help tooltipped downwards" title="Show command bar help">
    <span class="octicon octicon-question"></span>
  </span>


  <input type="hidden" name="ref" value="cmdform">

</form>
        <ul class="top-nav">
          <li class="explore"><a href="/explore">Explore</a></li>
            <li><a href="https://gist.github.com">Gist</a></li>
            <li><a href="/blog">Blog</a></li>
          <li><a href="https://help.github.com">Help</a></li>
        </ul>
      </div>

    


  <ul id="user-links">
    <li>
      <a href="/rodrigocruz" class="name">
        <img height="20" src="https://0.gravatar.com/avatar/b0a7f3a1314fee5cba737723ac0c59b3?d=https%3A%2F%2Fidenticons.github.com%2F36e237bc4c5b0020e7cc4d8d5d5f044a.png&amp;r=x&amp;s=140" width="20" /> rodrigocruz
      </a>
    </li>

      <li>
        <a href="/new" id="new_repo" class="tooltipped downwards" title="Create a new repo" aria-label="Create a new repo">
          <span class="octicon octicon-repo-create"></span>
        </a>
      </li>

      <li>
        <a href="/settings/profile" id="account_settings"
          class="tooltipped downwards"
          aria-label="Account settings "
          title="Account settings ">
          <span class="octicon octicon-tools"></span>
        </a>
      </li>
      <li>
        <a class="tooltipped downwards" href="/logout" data-method="post" id="logout" title="Sign out" aria-label="Sign out">
          <span class="octicon octicon-log-out"></span>
        </a>
      </li>

  </ul>

<div class="js-new-dropdown-contents hidden">
  

<ul class="dropdown-menu">
  <li>
    <a href="/new"><span class="octicon octicon-repo-create"></span> New repository</a>
  </li>
  <li>
    <a href="/organizations/new"><span class="octicon octicon-organization"></span> New organization</a>
  </li>



    <li class="section-title">
      <span title="jpo/vim-railscasts-theme">This repository</span>
    </li>
      <li>
        <a href="/jpo/vim-railscasts-theme/issues/new"><span class="octicon octicon-issue-opened"></span> New issue</a>
      </li>
</ul>

</div>


    
  </div>
</div>

      

      




          <div class="site" itemscope itemtype="http://schema.org/WebPage">
    
    <div class="pagehead repohead instapaper_ignore readability-menu">
      <div class="container">
        

<ul class="pagehead-actions">

    <li class="subscription">
      <form accept-charset="UTF-8" action="/notifications/subscribe" class="js-social-container" data-autosubmit="true" data-remote="true" method="post"><div style="margin:0;padding:0;display:inline"><input name="authenticity_token" type="hidden" value="6OSx6gZsal4pn6o+oVTJLD1runVTn28TL1Pudugt6HE=" /></div>  <input id="repository_id" name="repository_id" type="hidden" value="61883" />

    <div class="select-menu js-menu-container js-select-menu">
      <a class="social-count js-social-count" href="/jpo/vim-railscasts-theme/watchers">
        5
      </a>
      <span class="minibutton select-menu-button with-count js-menu-target" role="button" tabindex="0">
        <span class="js-select-button">
          <span class="octicon octicon-eye-watch"></span>
          Watch
        </span>
      </span>

      <div class="select-menu-modal-holder">
        <div class="select-menu-modal subscription-menu-modal js-menu-content">
          <div class="select-menu-header">
            <span class="select-menu-title">Notification status</span>
            <span class="octicon octicon-remove-close js-menu-close"></span>
          </div> <!-- /.select-menu-header -->

          <div class="select-menu-list js-navigation-container" role="menu">

            <div class="select-menu-item js-navigation-item selected" role="menuitem" tabindex="0">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <div class="select-menu-item-text">
                <input checked="checked" id="do_included" name="do" type="radio" value="included" />
                <h4>Not watching</h4>
                <span class="description">You only receive notifications for discussions in which you participate or are @mentioned.</span>
                <span class="js-select-button-text hidden-select-button-text">
                  <span class="octicon octicon-eye-watch"></span>
                  Watch
                </span>
              </div>
            </div> <!-- /.select-menu-item -->

            <div class="select-menu-item js-navigation-item " role="menuitem" tabindex="0">
              <span class="select-menu-item-icon octicon octicon octicon-check"></span>
              <div class="select-menu-item-text">
                <input id="do_subscribed" name="do" type="radio" value="subscribed" />
                <h4>Watching</h4>
                <span class="description">You receive notifications for all discussions in this repository.</span>
                <span class="js-select-button-text hidden-select-button-text">
                  <span class="octicon octicon-eye-unwatch"></span>
                  Unwatch
                </span>
              </div>
            </div> <!-- /.select-menu-item -->

            <div class="select-menu-item js-navigation-item " role="menuitem" tabindex="0">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <div class="select-menu-item-text">
                <input id="do_ignore" name="do" type="radio" value="ignore" />
                <h4>Ignoring</h4>
                <span class="description">You do not receive any notifications for discussions in this repository.</span>
                <span class="js-select-button-text hidden-select-button-text">
                  <span class="octicon octicon-mute"></span>
                  Stop ignoring
                </span>
              </div>
            </div> <!-- /.select-menu-item -->

          </div> <!-- /.select-menu-list -->

        </div> <!-- /.select-menu-modal -->
      </div> <!-- /.select-menu-modal-holder -->
    </div> <!-- /.select-menu -->

</form>
    </li>

  <li>
  
<div class="js-toggler-container js-social-container starring-container ">
  <a href="/jpo/vim-railscasts-theme/unstar" class="minibutton with-count js-toggler-target star-button starred upwards" title="Unstar this repo" data-remote="true" data-method="post" rel="nofollow">
    <span class="octicon octicon-star-delete"></span><span class="text">Unstar</span>
  </a>
  <a href="/jpo/vim-railscasts-theme/star" class="minibutton with-count js-toggler-target star-button unstarred upwards" title="Star this repo" data-remote="true" data-method="post" rel="nofollow">
    <span class="octicon octicon-star"></span><span class="text">Star</span>
  </a>
  <a class="social-count js-social-count" href="/jpo/vim-railscasts-theme/stargazers">138</a>
</div>

  </li>


        <li>
          <a href="/jpo/vim-railscasts-theme/fork" class="minibutton with-count js-toggler-target fork-button lighter upwards" title="Fork this repo" rel="nofollow" data-method="post">
            <span class="octicon octicon-git-branch-create"></span><span class="text">Fork</span>
          </a>
          <a href="/jpo/vim-railscasts-theme/network" class="social-count">63</a>
        </li>


</ul>

        <h1 itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="entry-title public">
          <span class="repo-label"><span>public</span></span>
          <span class="mega-octicon octicon-repo"></span>
          <span class="author">
            <a href="/jpo" class="url fn" itemprop="url" rel="author"><span itemprop="title">jpo</span></a>
          </span>
          <span class="repohead-name-divider">/</span>
          <strong><a href="/jpo/vim-railscasts-theme" class="js-current-repository js-repo-home-link">vim-railscasts-theme</a></strong>

          <span class="page-context-loader">
            <img alt="Octocat-spinner-32" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
          </span>

        </h1>
      </div><!-- /.container -->
    </div><!-- /.repohead -->

    <div class="container">

      <div class="repository-with-sidebar repo-container ">

        <div class="repository-sidebar">
            

<div class="sunken-menu vertical-right repo-nav js-repo-nav js-repository-container-pjax js-octicon-loaders">
  <div class="sunken-menu-contents">
    <ul class="sunken-menu-group">
      <li class="tooltipped leftwards" title="Code">
        <a href="/jpo/vim-railscasts-theme" aria-label="Code" class="selected js-selected-navigation-item sunken-menu-item" data-gotokey="c" data-pjax="true" data-selected-links="repo_source repo_downloads repo_commits repo_tags repo_branches /jpo/vim-railscasts-theme">
          <span class="octicon octicon-code"></span> <span class="full-word">Code</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

        <li class="tooltipped leftwards" title="Issues">
          <a href="/jpo/vim-railscasts-theme/issues" aria-label="Issues" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-gotokey="i" data-selected-links="repo_issues /jpo/vim-railscasts-theme/issues">
            <span class="octicon octicon-issue-opened"></span> <span class="full-word">Issues</span>
            <span class='counter'>1</span>
            <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>        </li>

      <li class="tooltipped leftwards" title="Pull Requests"><a href="/jpo/vim-railscasts-theme/pulls" aria-label="Pull Requests" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-gotokey="p" data-selected-links="repo_pulls /jpo/vim-railscasts-theme/pulls">
            <span class="octicon octicon-git-pull-request"></span> <span class="full-word">Pull Requests</span>
            <span class='counter'>1</span>
            <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>


    </ul>
    <div class="sunken-menu-separator"></div>
    <ul class="sunken-menu-group">

      <li class="tooltipped leftwards" title="Pulse">
        <a href="/jpo/vim-railscasts-theme/pulse" aria-label="Pulse" class="js-selected-navigation-item sunken-menu-item" data-pjax="true" data-selected-links="pulse /jpo/vim-railscasts-theme/pulse">
          <span class="octicon octicon-pulse"></span> <span class="full-word">Pulse</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

      <li class="tooltipped leftwards" title="Graphs">
        <a href="/jpo/vim-railscasts-theme/graphs" aria-label="Graphs" class="js-selected-navigation-item sunken-menu-item" data-pjax="true" data-selected-links="repo_graphs repo_contributors /jpo/vim-railscasts-theme/graphs">
          <span class="octicon octicon-graph"></span> <span class="full-word">Graphs</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

      <li class="tooltipped leftwards" title="Network">
        <a href="/jpo/vim-railscasts-theme/network" aria-label="Network" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-selected-links="repo_network /jpo/vim-railscasts-theme/network">
          <span class="octicon octicon-git-branch"></span> <span class="full-word">Network</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>
    </ul>


  </div>
</div>

            <div class="only-with-full-nav">
              

  

<div class="clone-url open"
  data-protocol-type="http"
  data-url="/users/set_protocol?protocol_selector=http&amp;protocol_type=clone">
  <h3><strong>HTTPS</strong> clone URL</h3>
  <div class="clone-url-box">
    <input type="text" class="clone js-url-field"
           value="https://github.com/jpo/vim-railscasts-theme.git" readonly="readonly">

    <span class="js-zeroclipboard url-box-clippy minibutton zeroclipboard-button" data-clipboard-text="https://github.com/jpo/vim-railscasts-theme.git" data-copied-hint="copied!" title="copy to clipboard"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>

  

<div class="clone-url "
  data-protocol-type="ssh"
  data-url="/users/set_protocol?protocol_selector=ssh&amp;protocol_type=clone">
  <h3><strong>SSH</strong> clone URL</h3>
  <div class="clone-url-box">
    <input type="text" class="clone js-url-field"
           value="git@github.com:jpo/vim-railscasts-theme.git" readonly="readonly">

    <span class="js-zeroclipboard url-box-clippy minibutton zeroclipboard-button" data-clipboard-text="git@github.com:jpo/vim-railscasts-theme.git" data-copied-hint="copied!" title="copy to clipboard"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>

  

<div class="clone-url "
  data-protocol-type="subversion"
  data-url="/users/set_protocol?protocol_selector=subversion&amp;protocol_type=clone">
  <h3><strong>Subversion</strong> checkout URL</h3>
  <div class="clone-url-box">
    <input type="text" class="clone js-url-field"
           value="https://github.com/jpo/vim-railscasts-theme" readonly="readonly">

    <span class="js-zeroclipboard url-box-clippy minibutton zeroclipboard-button" data-clipboard-text="https://github.com/jpo/vim-railscasts-theme" data-copied-hint="copied!" title="copy to clipboard"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>


<p class="clone-options">You can clone with
      <a href="#" class="js-clone-selector" data-protocol="http">HTTPS</a>,
      <a href="#" class="js-clone-selector" data-protocol="ssh">SSH</a>,
      or <a href="#" class="js-clone-selector" data-protocol="subversion">Subversion</a>.
  <span class="octicon help tooltipped upwards" title="Get help on which URL is right for you.">
    <a href="https://help.github.com/articles/which-remote-url-should-i-use">
    <span class="octicon octicon-question"></span>
    </a>
  </span>
</p>



              <a href="/jpo/vim-railscasts-theme/archive/master.zip"
                 class="minibutton sidebar-button"
                 title="Download this repository as a zip file"
                 rel="nofollow">
                <span class="octicon octicon-cloud-download"></span>
                Download ZIP
              </a>
            </div>
        </div><!-- /.repository-sidebar -->

        <div id="js-repo-pjax-container" class="repository-content context-loader-container" data-pjax-container>
          


<!-- blob contrib key: blob_contributors:v21:7a8c4329ccd024a5f2d642ba3476c10a -->

<p title="This is a placeholder element" class="js-history-link-replace hidden"></p>

<a href="/jpo/vim-railscasts-theme/find/master" data-pjax data-hotkey="t" class="js-show-file-finder" style="display:none">Show File Finder</a>

<div class="file-navigation">
  
  

<div class="select-menu js-menu-container js-select-menu" >
  <span class="minibutton select-menu-button js-menu-target" data-hotkey="w"
    data-master-branch="master"
    data-ref="master"
    role="button" aria-label="Switch branches or tags" tabindex="0">
    <span class="octicon octicon-git-branch"></span>
    <i>branch:</i>
    <span class="js-select-button">master</span>
  </span>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax>

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <span class="select-menu-title">Switch branches/tags</span>
        <span class="octicon octicon-remove-close js-menu-close"></span>
      </div> <!-- /.select-menu-header -->

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" aria-label="Filter branches/tags" id="context-commitish-filter-field" class="js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" class="js-select-menu-tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" class="js-select-menu-tab">Tags</a>
            </li>
          </ul>
        </div><!-- /.select-menu-tabs -->
      </div><!-- /.select-menu-filters -->

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <div class="select-menu-item js-navigation-item selected">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jpo/vim-railscasts-theme/blob/master/colors/railscasts.vim"
                 data-name="master"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="master">master</a>
            </div> <!-- /.select-menu-item -->
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div> <!-- /.select-menu-list -->

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div> <!-- /.select-menu-list -->

    </div> <!-- /.select-menu-modal -->
  </div> <!-- /.select-menu-modal-holder -->
</div> <!-- /.select-menu -->

  <div class="breadcrumb">
    <span class='repo-root js-repo-root'><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/jpo/vim-railscasts-theme" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">vim-railscasts-theme</span></a></span></span><span class="separator"> / </span><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/jpo/vim-railscasts-theme/tree/master/colors" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">colors</span></a></span><span class="separator"> / </span><strong class="final-path">railscasts.vim</strong> <span class="js-zeroclipboard minibutton zeroclipboard-button" data-clipboard-text="colors/railscasts.vim" data-copied-hint="copied!" title="copy to clipboard"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>



  <div class="commit file-history-tease">
    <img class="main-avatar" height="24" src="https://0.gravatar.com/avatar/730dcb0f4e855d47c14c0aa3c00b4f34?d=https%3A%2F%2Fidenticons.github.com%2Fe0a4794fc273e7d59334e5f4fba95f3b.png&amp;r=x&amp;s=140" width="24" />
    <span class="author"><a href="/antonr" rel="author">antonr</a></span>
    <time class="js-relative-date" datetime="2013-02-10T06:56:24-08:00" title="2013-02-10 06:56:24">February 10, 2013</time>
    <div class="commit-title">
        <a href="/jpo/vim-railscasts-theme/commit/a8c749e9feef0577ee8bc92960fdf05e2021a8f4" class="message" data-pjax="true" title="adding colors for terminal vim">adding colors for terminal vim</a>
    </div>

    <div class="participation">
      <p class="quickstat"><a href="#blob_contributors_box" rel="facebox"><strong>4</strong> contributors</a></p>
          <a class="avatar tooltipped downwards" title="jpo" href="/jpo/vim-railscasts-theme/commits/master/colors/railscasts.vim?author=jpo"><img height="20" src="https://0.gravatar.com/avatar/f550a08f4ff877f9c213bb46ea22c492?d=https%3A%2F%2Fidenticons.github.com%2F3b0de458c502917011cdb5292f078c7a.png&amp;r=x&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped downwards" title="drmohundro" href="/jpo/vim-railscasts-theme/commits/master/colors/railscasts.vim?author=drmohundro"><img height="20" src="https://1.gravatar.com/avatar/a2dfc63c765df477b9b7c7053b0a4369?d=https%3A%2F%2Fidenticons.github.com%2Fe008edf12132e41485fc702ed94c5943.png&amp;r=x&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped downwards" title="antonr" href="/jpo/vim-railscasts-theme/commits/master/colors/railscasts.vim?author=antonr"><img height="20" src="https://0.gravatar.com/avatar/730dcb0f4e855d47c14c0aa3c00b4f34?d=https%3A%2F%2Fidenticons.github.com%2Fe0a4794fc273e7d59334e5f4fba95f3b.png&amp;r=x&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped downwards" title="andersonfreitas" href="/jpo/vim-railscasts-theme/commits/master/colors/railscasts.vim?author=andersonfreitas"><img height="20" src="https://2.gravatar.com/avatar/71bdeefe64956a763aa5cccb85855d52?d=https%3A%2F%2Fidenticons.github.com%2Fb0af04fc89591e79ad1ff07211d994ed.png&amp;r=x&amp;s=140" width="20" /></a>


    </div>
    <div id="blob_contributors_box" style="display:none">
      <h2 class="facebox-header">Users who have contributed to this file</h2>
      <ul class="facebox-user-list">
          <li class="facebox-user-list-item">
            <img height="24" src="https://0.gravatar.com/avatar/f550a08f4ff877f9c213bb46ea22c492?d=https%3A%2F%2Fidenticons.github.com%2F3b0de458c502917011cdb5292f078c7a.png&amp;r=x&amp;s=140" width="24" />
            <a href="/jpo">jpo</a>
          </li>
          <li class="facebox-user-list-item">
            <img height="24" src="https://1.gravatar.com/avatar/a2dfc63c765df477b9b7c7053b0a4369?d=https%3A%2F%2Fidenticons.github.com%2Fe008edf12132e41485fc702ed94c5943.png&amp;r=x&amp;s=140" width="24" />
            <a href="/drmohundro">drmohundro</a>
          </li>
          <li class="facebox-user-list-item">
            <img height="24" src="https://0.gravatar.com/avatar/730dcb0f4e855d47c14c0aa3c00b4f34?d=https%3A%2F%2Fidenticons.github.com%2Fe0a4794fc273e7d59334e5f4fba95f3b.png&amp;r=x&amp;s=140" width="24" />
            <a href="/antonr">antonr</a>
          </li>
          <li class="facebox-user-list-item">
            <img height="24" src="https://2.gravatar.com/avatar/71bdeefe64956a763aa5cccb85855d52?d=https%3A%2F%2Fidenticons.github.com%2Fb0af04fc89591e79ad1ff07211d994ed.png&amp;r=x&amp;s=140" width="24" />
            <a href="/andersonfreitas">andersonfreitas</a>
          </li>
      </ul>
    </div>
  </div>

<div id="files" class="bubble">
  <div class="file">
    <div class="meta">
      <div class="info">
        <span class="icon"><b class="octicon octicon-file-text"></b></span>
        <span class="mode" title="File Mode">file</span>
          <span>136 lines (106 sloc)</span>
        <span>4.219 kb</span>
      </div>
      <div class="actions">
        <div class="button-group">
                <a class="minibutton tooltipped upwards"
                   title="Clicking this button will automatically fork this project so you can edit the file"
                   href="/jpo/vim-railscasts-theme/edit/master/colors/railscasts.vim"
                   data-method="post" rel="nofollow">Edit</a>
          <a href="/jpo/vim-railscasts-theme/raw/master/colors/railscasts.vim" class="button minibutton " id="raw-url">Raw</a>
            <a href="/jpo/vim-railscasts-theme/blame/master/colors/railscasts.vim" class="button minibutton ">Blame</a>
          <a href="/jpo/vim-railscasts-theme/commits/master/colors/railscasts.vim" class="button minibutton " rel="nofollow">History</a>
        </div><!-- /.button-group -->
          <a class="minibutton danger empty-icon tooltipped downwards"
             href="/jpo/vim-railscasts-theme/delete/master/colors/railscasts.vim"
             title="Fork this project and delete file"
             data-method="post" data-test-id="delete-blob-file" rel="nofollow">
          Delete
        </a>
      </div><!-- /.actions -->

    </div>
        <div class="blob-wrapper data type-viml js-blob-data">
        <table class="file-code file-diff">
          <tr class="file-code-line">
            <td class="blob-line-nums">
              <span id="L1" rel="#L1">1</span>
<span id="L2" rel="#L2">2</span>
<span id="L3" rel="#L3">3</span>
<span id="L4" rel="#L4">4</span>
<span id="L5" rel="#L5">5</span>
<span id="L6" rel="#L6">6</span>
<span id="L7" rel="#L7">7</span>
<span id="L8" rel="#L8">8</span>
<span id="L9" rel="#L9">9</span>
<span id="L10" rel="#L10">10</span>
<span id="L11" rel="#L11">11</span>
<span id="L12" rel="#L12">12</span>
<span id="L13" rel="#L13">13</span>
<span id="L14" rel="#L14">14</span>
<span id="L15" rel="#L15">15</span>
<span id="L16" rel="#L16">16</span>
<span id="L17" rel="#L17">17</span>
<span id="L18" rel="#L18">18</span>
<span id="L19" rel="#L19">19</span>
<span id="L20" rel="#L20">20</span>
<span id="L21" rel="#L21">21</span>
<span id="L22" rel="#L22">22</span>
<span id="L23" rel="#L23">23</span>
<span id="L24" rel="#L24">24</span>
<span id="L25" rel="#L25">25</span>
<span id="L26" rel="#L26">26</span>
<span id="L27" rel="#L27">27</span>
<span id="L28" rel="#L28">28</span>
<span id="L29" rel="#L29">29</span>
<span id="L30" rel="#L30">30</span>
<span id="L31" rel="#L31">31</span>
<span id="L32" rel="#L32">32</span>
<span id="L33" rel="#L33">33</span>
<span id="L34" rel="#L34">34</span>
<span id="L35" rel="#L35">35</span>
<span id="L36" rel="#L36">36</span>
<span id="L37" rel="#L37">37</span>
<span id="L38" rel="#L38">38</span>
<span id="L39" rel="#L39">39</span>
<span id="L40" rel="#L40">40</span>
<span id="L41" rel="#L41">41</span>
<span id="L42" rel="#L42">42</span>
<span id="L43" rel="#L43">43</span>
<span id="L44" rel="#L44">44</span>
<span id="L45" rel="#L45">45</span>
<span id="L46" rel="#L46">46</span>
<span id="L47" rel="#L47">47</span>
<span id="L48" rel="#L48">48</span>
<span id="L49" rel="#L49">49</span>
<span id="L50" rel="#L50">50</span>
<span id="L51" rel="#L51">51</span>
<span id="L52" rel="#L52">52</span>
<span id="L53" rel="#L53">53</span>
<span id="L54" rel="#L54">54</span>
<span id="L55" rel="#L55">55</span>
<span id="L56" rel="#L56">56</span>
<span id="L57" rel="#L57">57</span>
<span id="L58" rel="#L58">58</span>
<span id="L59" rel="#L59">59</span>
<span id="L60" rel="#L60">60</span>
<span id="L61" rel="#L61">61</span>
<span id="L62" rel="#L62">62</span>
<span id="L63" rel="#L63">63</span>
<span id="L64" rel="#L64">64</span>
<span id="L65" rel="#L65">65</span>
<span id="L66" rel="#L66">66</span>
<span id="L67" rel="#L67">67</span>
<span id="L68" rel="#L68">68</span>
<span id="L69" rel="#L69">69</span>
<span id="L70" rel="#L70">70</span>
<span id="L71" rel="#L71">71</span>
<span id="L72" rel="#L72">72</span>
<span id="L73" rel="#L73">73</span>
<span id="L74" rel="#L74">74</span>
<span id="L75" rel="#L75">75</span>
<span id="L76" rel="#L76">76</span>
<span id="L77" rel="#L77">77</span>
<span id="L78" rel="#L78">78</span>
<span id="L79" rel="#L79">79</span>
<span id="L80" rel="#L80">80</span>
<span id="L81" rel="#L81">81</span>
<span id="L82" rel="#L82">82</span>
<span id="L83" rel="#L83">83</span>
<span id="L84" rel="#L84">84</span>
<span id="L85" rel="#L85">85</span>
<span id="L86" rel="#L86">86</span>
<span id="L87" rel="#L87">87</span>
<span id="L88" rel="#L88">88</span>
<span id="L89" rel="#L89">89</span>
<span id="L90" rel="#L90">90</span>
<span id="L91" rel="#L91">91</span>
<span id="L92" rel="#L92">92</span>
<span id="L93" rel="#L93">93</span>
<span id="L94" rel="#L94">94</span>
<span id="L95" rel="#L95">95</span>
<span id="L96" rel="#L96">96</span>
<span id="L97" rel="#L97">97</span>
<span id="L98" rel="#L98">98</span>
<span id="L99" rel="#L99">99</span>
<span id="L100" rel="#L100">100</span>
<span id="L101" rel="#L101">101</span>
<span id="L102" rel="#L102">102</span>
<span id="L103" rel="#L103">103</span>
<span id="L104" rel="#L104">104</span>
<span id="L105" rel="#L105">105</span>
<span id="L106" rel="#L106">106</span>
<span id="L107" rel="#L107">107</span>
<span id="L108" rel="#L108">108</span>
<span id="L109" rel="#L109">109</span>
<span id="L110" rel="#L110">110</span>
<span id="L111" rel="#L111">111</span>
<span id="L112" rel="#L112">112</span>
<span id="L113" rel="#L113">113</span>
<span id="L114" rel="#L114">114</span>
<span id="L115" rel="#L115">115</span>
<span id="L116" rel="#L116">116</span>
<span id="L117" rel="#L117">117</span>
<span id="L118" rel="#L118">118</span>
<span id="L119" rel="#L119">119</span>
<span id="L120" rel="#L120">120</span>
<span id="L121" rel="#L121">121</span>
<span id="L122" rel="#L122">122</span>
<span id="L123" rel="#L123">123</span>
<span id="L124" rel="#L124">124</span>
<span id="L125" rel="#L125">125</span>
<span id="L126" rel="#L126">126</span>
<span id="L127" rel="#L127">127</span>
<span id="L128" rel="#L128">128</span>
<span id="L129" rel="#L129">129</span>
<span id="L130" rel="#L130">130</span>
<span id="L131" rel="#L131">131</span>
<span id="L132" rel="#L132">132</span>
<span id="L133" rel="#L133">133</span>
<span id="L134" rel="#L134">134</span>
<span id="L135" rel="#L135">135</span>

            </td>
            <td class="blob-line-code">
                    <div class="highlight"><pre><div class='line' id='LC1'><span class="c">&quot; Vim color scheme</span></div><div class='line' id='LC2'><span class="c">&quot;</span></div><div class='line' id='LC3'><span class="c">&quot; Name:        railscast.vim</span></div><div class='line' id='LC4'><span class="c">&quot; Maintainer:  Josh O&#39;Rourke &lt;joshorourke@me.com&gt;</span></div><div class='line' id='LC5'><span class="c">&quot; License:     public domain</span></div><div class='line' id='LC6'><span class="c">&quot;</span></div><div class='line' id='LC7'><span class="c">&quot; A GUI Only port of the RailsCasts TextMate theme [1] to Vim.</span></div><div class='line' id='LC8'><span class="c">&quot; Some parts of this theme were borrowed from the well-documented Lucius theme [2].</span></div><div class='line' id='LC9'><span class="c">&quot;</span></div><div class='line' id='LC10'><span class="c">&quot; [1] http://railscasts.com/about</span></div><div class='line' id='LC11'><span class="c">&quot; [2] http://www.vim.org/scripts/script.php?script_id=2536</span></div><div class='line' id='LC12'><br/></div><div class='line' id='LC13'><span class="k">set</span> <span class="nb">background</span><span class="p">=</span><span class="nb">dark</span></div><div class='line' id='LC14'><span class="k">hi</span> clear</div><div class='line' id='LC15'><span class="k">if</span> exists<span class="p">(</span><span class="s2">&quot;syntax_on&quot;</span><span class="p">)</span></div><div class='line' id='LC16'>&nbsp;&nbsp;<span class="nb">syntax</span> reset</div><div class='line' id='LC17'><span class="k">endif</span></div><div class='line' id='LC18'><span class="k">let</span> <span class="k">g</span>:colors_name <span class="p">=</span> <span class="s2">&quot;railscasts&quot;</span></div><div class='line' id='LC19'><br/></div><div class='line' id='LC20'><span class="c">&quot; Colors</span></div><div class='line' id='LC21'><span class="c">&quot; Brown        #BC9458</span></div><div class='line' id='LC22'><span class="c">&quot; Dark Blue    #6D9CBE</span></div><div class='line' id='LC23'><span class="c">&quot; Dark Green   #519F50</span></div><div class='line' id='LC24'><span class="c">&quot; Dark Orange  #CC7833</span></div><div class='line' id='LC25'><span class="c">&quot; Light Blue   #D0D0FF</span></div><div class='line' id='LC26'><span class="c">&quot; Light Green  #A5C261</span></div><div class='line' id='LC27'><span class="c">&quot; Tan          #FFC66D</span></div><div class='line' id='LC28'><br/></div><div class='line' id='LC29'><span class="k">hi</span> Normal                    guifg<span class="p">=</span>#E6E1DC guibg<span class="p">=</span>#<span class="m">2</span>B<span class="m">2</span>B<span class="m">2</span>B ctermfg<span class="p">=</span>white ctermbg<span class="p">=</span><span class="m">234</span></div><div class='line' id='LC30'><span class="k">hi</span> Cursor                    guifg<span class="p">=</span><span class="mh">#000000</span> guibg<span class="p">=</span>#FFFFFF ctermfg<span class="p">=</span><span class="m">0</span> ctermbg<span class="p">=</span><span class="m">15</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div class='line' id='LC31'><span class="k">hi</span> CursorLine                guibg<span class="p">=</span><span class="mh">#333435</span> ctermbg<span class="p">=</span><span class="m">235</span> cterm<span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC32'><span class="k">hi</span> Search                    guibg<span class="p">=</span>#<span class="m">5</span>A<span class="m">647</span>E ctermfg<span class="p">=</span><span class="nb">NONE</span> ctermbg<span class="p">=</span><span class="m">236</span> cterm<span class="p">=</span><span class="nb">underline</span></div><div class='line' id='LC33'><span class="k">hi</span> Visual                    guibg<span class="p">=</span>#<span class="m">5</span>A<span class="m">647</span>E ctermbg<span class="p">=</span><span class="m">60</span></div><div class='line' id='LC34'><span class="k">hi</span> LineNr                    guifg<span class="p">=</span><span class="mh">#888888</span> ctermfg<span class="p">=</span><span class="m">242</span></div><div class='line' id='LC35'><span class="k">hi</span> StatusLine                guibg<span class="p">=</span><span class="mh">#414243</span> <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span> guifg<span class="p">=</span>#E6E1DC</div><div class='line' id='LC36'><span class="k">hi</span> StatusLineNC              guibg<span class="p">=</span><span class="mh">#414243</span> <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC37'><span class="k">hi</span> VertSplit                 guibg<span class="p">=</span><span class="mh">#414243</span> <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span> guifg<span class="p">=</span><span class="mh">#414243</span></div><div class='line' id='LC38'><span class="k">hi</span> CursorLineNr              guifg<span class="p">=</span><span class="mh">#bbbbbb</span> ctermfg<span class="p">=</span><span class="m">248</span></div><div class='line' id='LC39'><span class="k">hi</span> ColorColumn               guibg<span class="p">=</span><span class="mh">#333435</span> ctermbg<span class="p">=</span><span class="m">235</span></div><div class='line' id='LC40'><br/></div><div class='line' id='LC41'><span class="c">&quot; Folds</span></div><div class='line' id='LC42'><span class="c">&quot; -----</span></div><div class='line' id='LC43'><span class="c">&quot; line used for closed folds</span></div><div class='line' id='LC44'><span class="k">hi</span> Folded                    guifg<span class="p">=</span>#F6F3E8 guibg<span class="p">=</span><span class="mh">#444444</span> <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC45'><br/></div><div class='line' id='LC46'><span class="c">&quot; Invisible Characters</span></div><div class='line' id='LC47'><span class="c">&quot; ------------------</span></div><div class='line' id='LC48'><span class="k">hi</span> NonText                   guifg<span class="p">=</span><span class="mh">#777777</span> <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC49'><span class="k">hi</span> SpecialKey                guifg<span class="p">=</span><span class="mh">#777777</span> <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC50'><br/></div><div class='line' id='LC51'><span class="c">&quot; Misc</span></div><div class='line' id='LC52'><span class="c">&quot; ----</span></div><div class='line' id='LC53'><span class="c">&quot; directory names and other special names in listings</span></div><div class='line' id='LC54'><span class="k">hi</span> Directory                 guifg<span class="p">=</span>#A5C261 <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC55'><br/></div><div class='line' id='LC56'><span class="c">&quot; Popup Menu</span></div><div class='line' id='LC57'><span class="c">&quot; ----------</span></div><div class='line' id='LC58'><span class="c">&quot; normal item in popup</span></div><div class='line' id='LC59'><span class="k">hi</span> Pmenu                     guifg<span class="p">=</span>#F6F3E8 guibg<span class="p">=</span><span class="mh">#444444</span> <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC60'><span class="c">&quot; selected item in popup</span></div><div class='line' id='LC61'><span class="k">hi</span> PmenuSel                  guifg<span class="p">=</span><span class="mh">#000000</span> guibg<span class="p">=</span>#A5C261 <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC62'><span class="c">&quot; scrollbar in popup</span></div><div class='line' id='LC63'><span class="k">hi</span> PMenuSbar                 guibg<span class="p">=</span>#<span class="m">5</span>A<span class="m">647</span>E <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC64'><span class="c">&quot; thumb of the scrollbar in the popup</span></div><div class='line' id='LC65'><span class="k">hi</span> PMenuThumb                guibg<span class="p">=</span>#AAAAAA <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC66'><br/></div><div class='line' id='LC67'><br/></div><div class='line' id='LC68'><span class="c">&quot;rubyComment</span></div><div class='line' id='LC69'><span class="k">hi</span> Comment                   guifg<span class="p">=</span>#BC9458 <span class="k">gui</span><span class="p">=</span><span class="nb">italic</span> ctermfg<span class="p">=</span><span class="m">137</span></div><div class='line' id='LC70'><span class="k">hi</span> Todo                      guifg<span class="p">=</span>#BC9458 guibg<span class="p">=</span><span class="nb">NONE</span> <span class="k">gui</span><span class="p">=</span><span class="nb">italic</span> ctermfg<span class="p">=</span><span class="m">94</span></div><div class='line' id='LC71'><br/></div><div class='line' id='LC72'><span class="c">&quot;rubyPseudoVariable</span></div><div class='line' id='LC73'><span class="c">&quot;nil, self, symbols, etc</span></div><div class='line' id='LC74'><span class="k">hi</span> Constant                  guifg<span class="p">=</span>#<span class="m">6</span>D<span class="m">9</span>CBE ctermfg<span class="p">=</span><span class="m">73</span></div><div class='line' id='LC75'><br/></div><div class='line' id='LC76'><span class="c">&quot;rubyClass, rubyModule, rubyDefine</span></div><div class='line' id='LC77'><span class="c">&quot;def, end, include, etc</span></div><div class='line' id='LC78'><span class="k">hi</span> Define                    guifg<span class="p">=</span>#CC7833 ctermfg<span class="p">=</span><span class="m">173</span></div><div class='line' id='LC79'><br/></div><div class='line' id='LC80'><span class="c">&quot;rubyInterpolation</span></div><div class='line' id='LC81'><span class="k">hi</span> Delimiter                 guifg<span class="p">=</span>#<span class="m">519</span>F<span class="m">50</span></div><div class='line' id='LC82'><br/></div><div class='line' id='LC83'><span class="c">&quot;rubyError, rubyInvalidVariable</span></div><div class='line' id='LC84'><span class="k">hi</span> <span class="k">Error</span>                     guifg<span class="p">=</span>#FFFFFF guibg<span class="p">=</span><span class="mh">#990000</span> ctermfg<span class="p">=</span><span class="m">221</span> ctermbg<span class="p">=</span><span class="m">88</span></div><div class='line' id='LC85'><br/></div><div class='line' id='LC86'><span class="c">&quot;rubyFunction</span></div><div class='line' id='LC87'><span class="k">hi</span> Function                  guifg<span class="p">=</span>#FFC66D <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span> ctermfg<span class="p">=</span><span class="m">221</span> cterm<span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC88'><br/></div><div class='line' id='LC89'><span class="c">&quot;rubyIdentifier</span></div><div class='line' id='LC90'><span class="c">&quot;@var, @@var, $var, etc</span></div><div class='line' id='LC91'><span class="k">hi</span> Identifier                guifg<span class="p">=</span>#D0D0FF <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span> ctermfg<span class="p">=</span><span class="m">73</span> cterm<span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC92'><br/></div><div class='line' id='LC93'><span class="c">&quot;rubyInclude</span></div><div class='line' id='LC94'><span class="c">&quot;include, autoload, extend, load, require</span></div><div class='line' id='LC95'><span class="k">hi</span> Include                   guifg<span class="p">=</span>#CC7833 <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span> ctermfg<span class="p">=</span><span class="m">173</span> cterm<span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC96'><br/></div><div class='line' id='LC97'><span class="c">&quot;rubyKeyword, rubyKeywordAsMethod</span></div><div class='line' id='LC98'><span class="c">&quot;alias, undef, super, yield, callcc, caller, lambda, proc</span></div><div class='line' id='LC99'><span class="k">hi</span> Keyword                   guifg<span class="p">=</span>#CC7833 ctermfg<span class="p">=</span><span class="m">172</span> cterm<span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC100'><br/></div><div class='line' id='LC101'><span class="c">&quot; same as define</span></div><div class='line' id='LC102'><span class="k">hi</span> Macro                     guifg<span class="p">=</span>#CC7833 <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span> ctermfg<span class="p">=</span><span class="m">172</span></div><div class='line' id='LC103'><br/></div><div class='line' id='LC104'><span class="c">&quot;rubyInteger</span></div><div class='line' id='LC105'><span class="k">hi</span> Number                    guifg<span class="p">=</span>#A5C261 ctermfg<span class="p">=</span><span class="m">107</span></div><div class='line' id='LC106'><br/></div><div class='line' id='LC107'><span class="c">&quot; #if, #else, #endif</span></div><div class='line' id='LC108'><span class="k">hi</span> PreCondit                 guifg<span class="p">=</span>#CC7833 <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span> ctermfg<span class="p">=</span><span class="m">172</span> cterm<span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC109'><br/></div><div class='line' id='LC110'><span class="c">&quot; generic preprocessor</span></div><div class='line' id='LC111'><span class="k">hi</span> PreProc                   guifg<span class="p">=</span>#CC7833 <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span> ctermfg<span class="p">=</span><span class="m">103</span></div><div class='line' id='LC112'><br/></div><div class='line' id='LC113'><span class="c">&quot;rubyControl, rubyAccess, rubyEval</span></div><div class='line' id='LC114'><span class="c">&quot;case, begin, do, for, if unless, while, until else, etc.</span></div><div class='line' id='LC115'><span class="k">hi</span> Statement                 guifg<span class="p">=</span>#CC7833 <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span> ctermfg<span class="p">=</span><span class="m">172</span> cterm<span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC116'><br/></div><div class='line' id='LC117'><span class="c">&quot;rubyString</span></div><div class='line' id='LC118'><span class="k">hi</span> String                    guifg<span class="p">=</span>#A5C261 ctermfg<span class="p">=</span><span class="m">107</span></div><div class='line' id='LC119'><br/></div><div class='line' id='LC120'><span class="k">hi</span> Title                     guifg<span class="p">=</span>#FFFFFF ctermfg<span class="p">=</span><span class="m">15</span></div><div class='line' id='LC121'><br/></div><div class='line' id='LC122'><span class="c">&quot;rubyConstant</span></div><div class='line' id='LC123'><span class="k">hi</span> Type                      guifg<span class="p">=</span>#DA4939 <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC124'><br/></div><div class='line' id='LC125'><span class="k">hi</span> DiffAdd                   guifg<span class="p">=</span>#E6E1DC guibg<span class="p">=</span><span class="mh">#144212</span></div><div class='line' id='LC126'><span class="k">hi</span> DiffDelete                guifg<span class="p">=</span>#E6E1DC guibg<span class="p">=</span><span class="mh">#660000</span></div><div class='line' id='LC127'><br/></div><div class='line' id='LC128'><span class="k">hi</span> link htmlTag              xmlTag</div><div class='line' id='LC129'><span class="k">hi</span> link htmlTagName          xmlTagName</div><div class='line' id='LC130'><span class="k">hi</span> link htmlEndTag           xmlEndTag</div><div class='line' id='LC131'><br/></div><div class='line' id='LC132'><span class="k">hi</span> xmlTag                    guifg<span class="p">=</span>#E8BF6A</div><div class='line' id='LC133'><span class="k">hi</span> xmlTagName                guifg<span class="p">=</span>#E8BF6A</div><div class='line' id='LC134'><span class="k">hi</span> xmlEndTag                 guifg<span class="p">=</span>#E8BF6A</div><div class='line' id='LC135'><br/></div></pre></div>
            </td>
          </tr>
        </table>
  </div>

  </div>
</div>

<a href="#jump-to-line" rel="facebox[.linejump]" data-hotkey="l" class="js-jump-to-line" style="display:none">Jump to Line</a>
<div id="jump-to-line" style="display:none">
  <form accept-charset="UTF-8" class="js-jump-to-line-form">
    <input class="linejump-input js-jump-to-line-field" type="text" placeholder="Jump to line&hellip;" autofocus>
    <button type="submit" class="button">Go</button>
  </form>
</div>

        </div>

      </div><!-- /.repo-container -->
      <div class="modal-backdrop"></div>
    </div><!-- /.container -->
  </div><!-- /.site -->


    </div><!-- /.wrapper -->

      <div class="container">
  <div class="site-footer">
    <ul class="site-footer-links right">
      <li><a href="https://status.github.com/">Status</a></li>
      <li><a href="http://developer.github.com">API</a></li>
      <li><a href="http://training.github.com">Training</a></li>
      <li><a href="http://shop.github.com">Shop</a></li>
      <li><a href="/blog">Blog</a></li>
      <li><a href="/about">About</a></li>

    </ul>

    <a href="/">
      <span class="mega-octicon octicon-mark-github"></span>
    </a>

    <ul class="site-footer-links">
      <li>&copy; 2013 <span title="0.04746s from github-fe123-cp1-prd.iad.github.net">GitHub</span>, Inc.</li>
        <li><a href="/site/terms">Terms</a></li>
        <li><a href="/site/privacy">Privacy</a></li>
        <li><a href="/security">Security</a></li>
        <li><a href="/contact">Contact</a></li>
    </ul>
  </div><!-- /.site-footer -->
</div><!-- /.container -->


    <div class="fullscreen-overlay js-fullscreen-overlay" id="fullscreen_overlay">
  <div class="fullscreen-container js-fullscreen-container">
    <div class="textarea-wrap">
      <textarea name="fullscreen-contents" id="fullscreen-contents" class="js-fullscreen-contents" placeholder="" data-suggester="fullscreen_suggester"></textarea>
          <div class="suggester-container">
              <div class="suggester fullscreen-suggester js-navigation-container" id="fullscreen_suggester"
                 data-url="/jpo/vim-railscasts-theme/suggestions/commit">
              </div>
          </div>
    </div>
  </div>
  <div class="fullscreen-sidebar">
    <a href="#" class="exit-fullscreen js-exit-fullscreen tooltipped leftwards" title="Exit Zen Mode">
      <span class="mega-octicon octicon-screen-normal"></span>
    </a>
    <a href="#" class="theme-switcher js-theme-switcher tooltipped leftwards"
      title="Switch themes">
      <span class="octicon octicon-color-mode"></span>
    </a>
  </div>
</div>



    <div id="ajax-error-message" class="flash flash-error">
      <span class="octicon octicon-alert"></span>
      <a href="#" class="octicon octicon-remove-close close ajax-error-dismiss"></a>
      Something went wrong with that request. Please try again.
    </div>

  </body>
</html>

